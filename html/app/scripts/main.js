$(document).ready(function() {

  $('.home-slider').slick({
    infinite: true,
    // autoplay: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });

  $('.multiple-items').slick({
    infinite: true,
    slidesToShow: 2,
    draggable:true,
    slidesToScroll: 1,
    prevArrow: '<div class="prev"><i class="fas fa-angle-left"></i></div>',
    nextArrow: '<div class="next"><i class="fas fa-angle-right"></i></div>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $('.fitur-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows:true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows:true,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          arrows:true,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          arrows:true,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  
  $('.fitur-items').on('wheel', (function(e) {
    e.preventDefault();
    if (e.originalEvent.deltaY < 0) {
      $(this).slick('slickNext');
    } else {
      $(this).slick('slickPrev');
    }
  }));

  $('.product-slider').slick({
    arrows: false,
    dots: true,
    speed: 500,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });

  $('.navbar-nav').on('click', 'li', function() {
    $('.navbar-nav li.active').removeClass('active');
    $(this).addClass('active');
  });

  new WOW().init();

  function toggleIcon(e) {
  $(e.target)
      .prev('.panel-heading')
      .find('.more-less')
      .toggleClass('fa-plus fa-times');
  }
  $('.panel-group').on('hidden.bs.collapse', toggleIcon);
  $('.panel-group').on('shown.bs.collapse', toggleIcon);

  $('.reveal').on('click',function() {
    var $pwd = $('.pwd');
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
  });

 

  $('.multiple-items').on('init', function(event, slick){
    //init code goes here
  }).on('afterChange',function(e,o){
      //on change slide = do action
      $('iframe').each(function(){
          $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
      });
  }).slick();

  

});  
